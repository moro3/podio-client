package com.moro.podio.utils;

import android.util.Base64;

import com.moro.podio.net.api.AuthApi;

import java.nio.charset.Charset;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 22.12.14
 * Time: 22:56
 */
public class Utils {

	private Utils(){}

	public static String getApiSecret() {
		return new String(Base64.decode(AuthApi.API_SECRET, Base64.DEFAULT), Charset.forName("UTF-8"));
	}

}
