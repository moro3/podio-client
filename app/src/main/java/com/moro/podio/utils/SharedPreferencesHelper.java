package com.moro.podio.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.moro.podio.PodioApplication;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 15:16
 */
public class SharedPreferencesHelper {

	private static final String TAG = SharedPreferencesHelper.class.getSimpleName();

	private static SharedPreferences sharedPreferences;

	public static void init() {
		Log.d(TAG, "Shared prefs helper init()");
		Context context = PodioApplication.getInstance().getApplicationContext();
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public static void saveAccessToken(String accessToken) {
		sharedPreferences.edit().putString(SharedPreferencesKeys.KEY_ACCESS_TOKEN, accessToken).apply();
	}

	public static String loadAccessToken() {
		return sharedPreferences.getString(SharedPreferencesKeys.KEY_ACCESS_TOKEN, "");
	}

	private interface SharedPreferencesKeys {
		String KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN";
	}

}
