package com.moro.podio.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.applidium.headerlistview.SectionAdapter;
import com.moro.podio.R;
import com.moro.podio.net.pojo.OrganizationResponseContainer;

import java.util.ArrayList;
import java.util.List;

import static com.moro.podio.net.pojo.OrganizationResponseContainer.Spaces;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 23.12.14
 * Time: 21:00
 */
public class WorkspaceListAdapter extends SectionAdapter {

	private Context mContext;
	/**
	 * Storing toast instance to prevent toast stacking.
	 */
	//TODO Move to activity and propagate onRowItemClick through listener
	private Toast mToast;

	private List<OrganizationResponseContainer> mData;

	public WorkspaceListAdapter(Context context) {
		this.mContext = context;
		mData = new ArrayList<>();
	}

	@Override
	public int numberOfSections() {
		return mData.size();
	}

	@Override
	public int numberOfRows(int section) {
		return mData.get(section).getSpaces().size();
	}

	@Override
	public View getRowView(int section, int row, View convertView, ViewGroup parent) {
		View view;
		ItemViewHolder holder;
		if (convertView == null) {
			view = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1, parent, false);
			holder = new ItemViewHolder();
			holder.textView = (TextView) view.findViewById(android.R.id.text1);
			view.setTag(holder);
		} else {
			view = convertView;
			holder = (ItemViewHolder) view.getTag();
		}
		Spaces rowItem = getRowItem(section, row);
		holder.textView.setText(rowItem.getName());
		return view;
	}

	@Override
	public Spaces getRowItem(int section, int row) {
		return mData.get(section).getSpaces().get(row);
	}

	@Override
	public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
		View view;
		SectionViewHolder holder;
		if (convertView == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.header_item, parent, false);
			holder = new SectionViewHolder();
			holder.textView = (TextView) view.findViewById(R.id.header_title);
			view.setTag(holder);
		} else {
			view = convertView;
			holder = (SectionViewHolder) view.getTag();
		}
		String headerName = getSectionHeaderItem(section);
		holder.textView.setText(headerName);
		return view;
	}

	@Override
	public String getSectionHeaderItem(int section) {
		return mData.get(section).getName();
	}

	@Override
	public boolean hasSectionHeaderView(int section) {
		return !mData.get(section).getSpaces().isEmpty();
	}

	@Override
	public void onRowItemClick(AdapterView<?> parent, View view, int section, int row, long id) {
		if (mToast != null) {
			mToast.cancel();
		}
		mToast = Toast.makeText(mContext, "Clicked: " + getRowItem(section, row).getName(), Toast.LENGTH_SHORT);
		mToast.show();
	}

	public void onNewDataRetrieved(List<OrganizationResponseContainer> data) {
		this.mData = data;
		notifyDataSetChanged();
	}

	class ItemViewHolder {
		TextView textView;
	}

	class SectionViewHolder {
		TextView textView;
	}
}
