package com.moro.podio.core;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.moro.podio.net.PodioApi;
import com.moro.podio.net.pojo.OrganizationResponseContainer;

import java.util.List;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 23.12.14
 * Time: 00:59
 */
public class WorkspaceListLoader extends AsyncTaskLoader<List<OrganizationResponseContainer>> {

	private List<OrganizationResponseContainer> organizationList;
	/**
	 * Stores away the application context associated with context.
	 * Since Loaders can be used across multiple activities it's dangerous to
	 * store the context directly; always use {@link #getContext()} to retrieve
	 * the Loader's Context, don't use the constructor argument directly.
	 * The Context returned by {@link #getContext} is safe to use across
	 * Activity instances.
	 *
	 * @param context used to retrieve the application context.
	 */
	public WorkspaceListLoader(Context context) {
		super(context);
	}

	@Override
	protected void onStartLoading() {
		if (organizationList != null) {
			deliverResult(organizationList);
		}

		if (organizationList == null || takeContentChanged()) {
			forceLoad();
		}
	}

	@Override
	public List<OrganizationResponseContainer> loadInBackground() {
		return PodioApi.core.getOrganizations();
	}

	@Override
	protected void onReset() {
		organizationList = null;
	}

	@Override
	public void deliverResult(List<OrganizationResponseContainer> data) {
		if (isReset()) {
			organizationList = null;
			return;
		}

		organizationList = data;

		if (isStarted()) {
			super.deliverResult(data);
		}
	}
}
