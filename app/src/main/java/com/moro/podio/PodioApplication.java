package com.moro.podio;

import android.app.Application;
import android.util.Log;

import com.moro.podio.net.PodioApi;
import com.moro.podio.utils.SharedPreferencesHelper;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 14:22
 */
public class PodioApplication extends Application {

	private static final String TAG = PodioApplication.class.getSimpleName();

	private static PodioApplication instance;

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "Application onCreate()");
		instance = this;
		SharedPreferencesHelper.init();
		PodioApi.init();
	}

	public static PodioApplication getInstance() {
		return instance;
	}
}
