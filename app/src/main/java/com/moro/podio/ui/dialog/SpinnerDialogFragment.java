package com.moro.podio.ui.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 24.12.14
 * Time: 01:09
 */
public class SpinnerDialogFragment extends DialogFragment {

	private static final String KEY_DIALOG_MESSAGE = "KEY_DIALOG_MESSAGE";

	public static final String FRAGMENT_TAG = SpinnerDialogFragment.class.getSimpleName();

	public static SpinnerDialogFragment newInstance(String message, boolean cancelable) {
		SpinnerDialogFragment fragment = new SpinnerDialogFragment();
		Bundle args = new Bundle();
		args.putString(KEY_DIALOG_MESSAGE, message);
		fragment.setArguments(args);
		fragment.setCancelable(cancelable);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String message = getArguments().getString(KEY_DIALOG_MESSAGE);
		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setMessage(message == null ? "" : message);
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}

}
