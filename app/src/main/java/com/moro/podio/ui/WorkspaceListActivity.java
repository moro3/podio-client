package com.moro.podio.ui;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.applidium.headerlistview.HeaderListView;
import com.moro.podio.R;
import com.moro.podio.core.WorkspaceListAdapter;
import com.moro.podio.core.WorkspaceListLoader;
import com.moro.podio.net.pojo.OrganizationResponseContainer;
import com.moro.podio.ui.dialog.SpinnerDialogFragment;

import java.util.List;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 17:38
 */
public class WorkspaceListActivity extends ActionBarActivity {

	private static final String TAG = WorkspaceListActivity.class.getSimpleName();

	private final int LOADER_ID = 10001;

	private SpinnerDialogFragment mSpinnerDialog;


	private LoaderManager.LoaderCallbacks<List<OrganizationResponseContainer>> mLoaderCallbacks =
			new LoaderManager.LoaderCallbacks<List<OrganizationResponseContainer>>() {

				@Override
				public Loader<List<OrganizationResponseContainer>> onCreateLoader(int id, Bundle args) {
					return new WorkspaceListLoader(getApplicationContext());
				}

				@Override
				public void onLoadFinished(Loader<List<OrganizationResponseContainer>> loader, List<OrganizationResponseContainer> data) {
					dismissSpinnerDialog();
					onOrganizationListRetrieved(data);
				}

				@Override
				public void onLoaderReset(Loader<List<OrganizationResponseContainer>> loader) {
					dismissSpinnerDialog();
				}
			};
	private WorkspaceListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		setContentView(R.layout.activity_workspaces_list);
		HeaderListView listView = (HeaderListView) findViewById(R.id.list_view);
		mAdapter = new WorkspaceListAdapter(this);
		listView.setAdapter(mAdapter);

		//Retrieve running spinner dialog (if any)
		mSpinnerDialog = (SpinnerDialogFragment) getFragmentManager().findFragmentByTag(SpinnerDialogFragment.FRAGMENT_TAG);

		getLoaderManager().initLoader(LOADER_ID, null, mLoaderCallbacks).startLoading();
		showSpinnerDialog(getString(R.string.loading_organizations));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_refresh:
				getLoaderManager().getLoader(LOADER_ID).forceLoad();
				showSpinnerDialog(getString(R.string.loading_organizations));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void onOrganizationListRetrieved(List<OrganizationResponseContainer> data) {
		mAdapter.onNewDataRetrieved(data);
	}

	private void showSpinnerDialog(String dialogText) {
		if (mSpinnerDialog == null) {
			mSpinnerDialog = SpinnerDialogFragment.newInstance(dialogText, false /* not cancellable */);
			mSpinnerDialog.show(getFragmentManager(), SpinnerDialogFragment.FRAGMENT_TAG);
		}
	}

	private void dismissSpinnerDialog() {
		if (mSpinnerDialog != null) {
			mSpinnerDialog.dismissAllowingStateLoss();
			mSpinnerDialog = null;
		}
	}
}
