package com.moro.podio.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.moro.podio.R;
import com.moro.podio.net.PodioApi;
import com.moro.podio.net.api.AuthApi;
import com.moro.podio.net.pojo.LoginResponseContainer;
import com.moro.podio.ui.dialog.SpinnerDialogFragment;
import com.moro.podio.utils.SharedPreferencesHelper;
import com.moro.podio.utils.Utils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends ActionBarActivity {

	private static final String TAG = LoginActivity.class.getSimpleName();

	private EditText mEmailField;
	private EditText mPasswordField;
	private Callback<LoginResponseContainer> mLoginResponseCallback;

	private SpinnerDialogFragment mSpinnerDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		setContentView(R.layout.activity_main);
		mEmailField = (EditText) findViewById(R.id.email_field);
		mPasswordField = (EditText) findViewById(R.id.password_field);
		// Pre-fill the fields for convenience
		mEmailField.setText("sebrehtest+puzzle@gmail.com");
		mPasswordField.setText("password1234");

		//Retrieve running spinner dialog (if any)
		mSpinnerDialog = (SpinnerDialogFragment) getFragmentManager().findFragmentByTag(SpinnerDialogFragment.FRAGMENT_TAG);
	}

	/**
	 * Login button click listener
	 * @param view
	 */
	public void onLoginButtonClick(View view) {
		if (mLoginResponseCallback == null) {
			mLoginResponseCallback = new Callback<LoginResponseContainer>() {
				@Override
				public void success(LoginResponseContainer loginResponseContainer, Response response) {
					Log.d(TAG, "Login success, showing workspace list");
					Toast.makeText(LoginActivity.this, "Logged in", Toast.LENGTH_SHORT).show();
					onLoginCompleted(true, loginResponseContainer.getAccessToken());
				}

				@Override
				public void failure(RetrofitError error) {
					Log.e(TAG, "Error: " + error.getMessage());
					Toast.makeText(LoginActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
					onLoginCompleted(false, null);
				}
			};
		}
		String userName = mEmailField.getText().toString();
		String password = mPasswordField.getText().toString();
		PodioApi.auth.login(userName, password, AuthApi.API_KEY, Utils.getApiSecret(), mLoginResponseCallback);
		showSpinnerDialog(getString(R.string.logging_in));
	}

	private void onLoginCompleted(boolean isSuccess, String accessToken) {
		dismissSpinnerDialog();
		if (isSuccess) {
			SharedPreferencesHelper.saveAccessToken(accessToken);
			startActivity(new Intent(LoginActivity.this, WorkspaceListActivity.class));
			finish();
		}
	}

	private void showSpinnerDialog(String dialogText) {
		if (mSpinnerDialog == null) {
			mSpinnerDialog = SpinnerDialogFragment.newInstance(dialogText, false /* not cancellable */);
			mSpinnerDialog.show(getFragmentManager(), SpinnerDialogFragment.FRAGMENT_TAG);
		}
	}

	private void dismissSpinnerDialog() {
		if (mSpinnerDialog != null && mSpinnerDialog.getFragmentManager() != null) {
			mSpinnerDialog.dismissAllowingStateLoss();
			mSpinnerDialog = null;
		}
	}
}
