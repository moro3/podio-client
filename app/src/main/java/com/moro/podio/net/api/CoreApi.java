package com.moro.podio.net.api;

import com.moro.podio.net.pojo.OrganizationResponseContainer;

import java.util.List;

import retrofit.http.GET;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 14:31
 */
public interface CoreApi {

	@GET("/org")
	public List<OrganizationResponseContainer> getOrganizationsList();

}
