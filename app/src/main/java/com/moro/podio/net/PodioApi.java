package com.moro.podio.net;

import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moro.podio.net.api.AuthApi;
import com.moro.podio.net.api.CoreApi;
import com.moro.podio.utils.SharedPreferencesHelper;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 13:58
 */
public class PodioApi {

	private static final String TAG = PodioApi.class.getSimpleName();

	public static final String API_URL = "https://api.podio.com";

	public static AuthApiManager auth;
	public static CoreApiManager core;

	public static void init(){
		Log.d(TAG, "Podio API init()");
		Gson gson = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
				.create();

		RestAdapter loginApiAdapter = new RestAdapter.Builder()
				.setEndpoint(API_URL)
				.setConverter(new GsonConverter(gson))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.build();

		auth = new AuthApiManager(loginApiAdapter.create(AuthApi.class));

		RestAdapter coreApiAdapter = new RestAdapter.Builder()
				.setEndpoint(API_URL)
				.setConverter(new GsonConverter(gson))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setRequestInterceptor(new RequestInterceptor() {
					@Override
					public void intercept(RequestFacade request) {
						request.addHeader("Authorization", "OAuth2 " + SharedPreferencesHelper.loadAccessToken());
					}
				})
				.build();

		core = new CoreApiManager(coreApiAdapter.create(CoreApi.class));
	}

}
