package com.moro.podio.net;

import com.moro.podio.net.api.AuthApi;
import com.moro.podio.net.pojo.LoginResponseContainer;

import retrofit.Callback;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 15:08
 */
public class AuthApiManager {

	private AuthApi authApi;

	public AuthApiManager(AuthApi authApi) {
		this.authApi = authApi;
	}

	public void login(String userName, String password, String clientId, String clientSecret, Callback<LoginResponseContainer> callback) {
		authApi.login(AuthApi.GRANT_TYPE, userName, password, clientId, clientSecret, callback);
	}
}
