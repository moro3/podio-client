package com.moro.podio.net.pojo;

import java.util.ArrayList;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 23.12.14
 * Time: 00:52
 */
public class OrganizationResponseContainer {

	private String name;
	private ArrayList<Spaces> spaces;

	public static class Spaces {
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	public ArrayList<Spaces> getSpaces() {
		return spaces;
	}

	public void setSpaces(ArrayList<Spaces> spaces) {
		this.spaces = spaces;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
