package com.moro.podio.net;

import com.moro.podio.net.api.CoreApi;
import com.moro.podio.net.pojo.OrganizationResponseContainer;

import java.util.List;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 23.12.14
 * Time: 00:47
 */
public class CoreApiManager {

	private CoreApi coreApi;

	public CoreApiManager(CoreApi coreApi) {
		this.coreApi = coreApi;
	}

	public List<OrganizationResponseContainer> getOrganizations() {
		return coreApi.getOrganizationsList();
	}
}
