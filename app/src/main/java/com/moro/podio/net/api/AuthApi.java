package com.moro.podio.net.api;

import com.moro.podio.net.pojo.LoginResponseContainer;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created with Android Studio.
 * User: Vitalii Morozov
 * Date: 21.12.14
 * Time: 14:10
 */
public interface AuthApi {

	public static final String API_KEY = "podio-puzzle";
	public static final String API_SECRET = "MnU4YzVTYnlsaHZKMXV6ZVlNSXNOUzlmZVBBNmhsa0F5R3RHamxXYU4ycjlGclRoaGNtd0JoNjdFUEhVcENIZA==";

	public static final String GRANT_TYPE = "password";

	@FormUrlEncoded
	@POST("/oauth/token")
	public void login(@Field("grant_type") String grantType, @Field("username") String userName, @Field("password") String password,
	                  @Field("client_id") String clientId, @Field("client_secret") String clientSecret, Callback<LoginResponseContainer> callback);
}
