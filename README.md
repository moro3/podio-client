This is an implementation of Podio Mobile Developer Assignment.

**Installation**

Just clone the repo and open **build.gradle** from the root folder in Android Studio

**Notes on the implementation**

I've used several libs to complete this assignment:

* Retrofit with gson - to easily implement REST API request and response handling
* [HeaderListView](https://github.com/applidium/HeaderListView) - to show list view with sections. It's fits perfectly for this task because I don't need to re-structure the data, that comes from server in JSON, I just pass it to SectionedAdapter as is. This library had several issues, but I've fixed them and it works good. The other options, like MergeAdapter or SectionedItem/EntryItem, require creating separate adapter for every section and/or data re-structurization.

**Further improvements**

* Add caching of organizations response - CoreApiManager, that wraps CoreApi object, can be used to query and immediately callback with cache result, if available.
* Add ability to externally set onRowItemClickListener (now it should be overridden inside your SectionAdapter implementation)
* Add empty view to organizations list